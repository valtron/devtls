from typing import Any, Optional
from pathlib import Path

class ManualRootInstallRequired(Exception):
	def __init__(self, cert_path: Path, store: str) -> None:
		super().__init__()
		self.cert_path = cert_path
		self.store = store
	
	def __str__(self) -> str:
		return '\n'.join([
			"",
			"#",
			"# New root certificate created:",
			"#   {}".format(self.cert_path),
			"# It expires in 30 days.",
			"#",
			"# Automatic install not supported on this system.",
			"# {}".format(get_install_message(self.store)),
			"#",
			"# (Help appreciated! https://gitlab.com/valtron/devtls)"
			"#",
		])

def get_install_message(store: str) -> str:
	if store == 'java':
		return "You must add it to your Java keystore."
	if store == 'os_posix':
		return "You must add it to your certificate store."
	if store == 'nss':
		return "You must add it to your NSS certificate store (used by Firefox)."
	if store == 'nt':
		return "You must add it to Windows' \"Trusted Root Certification Authority\" store."
	if store == 'darwin':
		return "You must add it to your system keychain."
	
	return "You must add it to your certificate store."
