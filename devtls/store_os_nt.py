from typing import Any, Iterable, Optional
from datetime import datetime
from ctypes import c_void_p, c_char_p, c_bool, c_int32, POINTER, Structure, string_at, cast
from contextlib import contextmanager
from pathlib import Path

from . import ffi_util, crypto_util

def remove_old_and_install_new(name: str, p_crt: Path) -> None:
	from cryptography.hazmat.primitives import serialization
	from cryptography.hazmat.backends import default_backend
	
	crt = crypto_util.load_cert(p_crt)
	be = default_backend()
	
	with open_system_store('ROOT') as store_ptr:
		# Remove old certs
		found = False
		for cert_ptr in iter_store_certs(store_ptr):
			cert = try_parse_cert(cert_ptr, be)
			if cert is None: continue
			if cert.serial_number == crt.serial_number:
				found = True
				continue
			if get_common_name(cert.issuer) != name: continue
			if get_common_name(cert.subject) != name: continue
			res = crypt32.CertDeleteCertificateFromStore(crypt32.CertDuplicateCertificateContext(cert_ptr))
			ffi_util.raise_if_falsy(res)
		
		if not found:
			# Install new cert
			encoded = crt.public_bytes(serialization.Encoding.DER)
			res = crypt32.CertAddEncodedCertificateToStore(
				store_ptr, X509_ASN_ENCODING, cast(c_char_p(encoded), c_void_p), len(encoded), CERT_STORE_ADD_REPLACE_EXISTING, None
			)
			ffi_util.raise_if_falsy(res)

class CERT_CONTEXT(Structure):
	_fields_ = [
		('encoding_type', c_int32),
		('cert_encoded', c_void_p),
		('cert_len', c_int32),
	]
PCERT_CONTEXT = POINTER(CERT_CONTEXT)
HCERTSTORE = c_void_p

crypt32 = ffi_util.open('crypt32', {
	'CertOpenSystemStoreW': ([c_void_p, c_char_p], HCERTSTORE),
	'CertDuplicateCertificateContext': ([PCERT_CONTEXT], PCERT_CONTEXT),
	'CertCloseStore': ([HCERTSTORE, c_int32], c_bool),
	'CertEnumCertificatesInStore': ([HCERTSTORE, PCERT_CONTEXT], PCERT_CONTEXT),
	'CertAddEncodedCertificateToStore': ([HCERTSTORE, c_int32, c_void_p, c_int32, c_int32, PCERT_CONTEXT], c_bool),
	'CertDeleteCertificateFromStore': ([PCERT_CONTEXT], c_bool),
})

X509_ASN_ENCODING = 1
CERT_STORE_ADD_REPLACE_EXISTING = 3
CERT_STORE_PROV_SYSTEM_A = cast(9, c_char_p)
CERT_STORE_OPEN_EXISTING_FLAG = 16384
CERT_STORE_MAXIMUM_ALLOWED_FLAG = 4096
CERT_SYSTEM_STORE_CURRENT_USER = (1 << 16)
CERT_SYSTEM_STORE_LOCAL_MACHINE = (2 << 16)

@contextmanager
def open_system_store(name: str) -> Iterable[HCERTSTORE]:
	store_ptr = crypt32.CertOpenSystemStoreW(None, name.encode('utf-16-le') + b'\0\0')
	ffi_util.raise_if_falsy(store_ptr)
	try:
		yield store_ptr
	finally:
		ffi_util.raise_if_falsy(crypt32.CertCloseStore(store_ptr, 0))

def iter_store_certs(store_ptr: HCERTSTORE) -> Iterable[PCERT_CONTEXT]:
	cert_ptr = None
	while True:
		cert_ptr = crypt32.CertEnumCertificatesInStore(store_ptr, cert_ptr)
		if not cert_ptr: break
		yield cert_ptr

def try_parse_cert(cert_ptr: PCERT_CONTEXT, be: Any) -> Optional[Any]:
	cts = cert_ptr.contents
	if cts.encoding_type != X509_ASN_ENCODING: return None
	data = string_at(cts.cert_encoded, cts.cert_len)
	return be.load_der_x509_certificate(data)

def get_common_name(name: Any) -> Optional[str]:
	from cryptography.x509.oid import NameOID
	attrs = name.get_attributes_for_oid(NameOID.COMMON_NAME)
	if not attrs: return None
	return attrs[0].value
