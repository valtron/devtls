from typing import Dict, List, Tuple, Any
import ctypes

CType = Any

class Lib: pass

def open(name: str, funcs: Dict[str, Tuple[List[CType], CType]]) -> Any:
	lib = ctypes.cdll.LoadLibrary(name)
	obj = Lib()
	for name, (argtypes, restype) in funcs.items():
		func = getattr(lib, name)
		func.argtypes = argtypes
		func.restype = restype
		setattr(obj, name, func)
	return obj

def raise_if_falsy(obj: Any) -> None:
	if not obj:
		raise ctypes.WinError()
