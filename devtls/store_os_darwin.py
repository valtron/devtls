from typing import Any
from pathlib import Path

from .errors import ManualRootInstallRequired

def remove_old_and_install_new(name: str, p_crt: Path) -> None:
	raise ManualRootInstallRequired(p_crt, 'os_darwin')
