from pathlib import Path
from aiohttp import web
from devtls import DevTLS

def main() -> None:
	devtls = DevTLS('devtls.server', cert_dir = Path.home() / '.devtls_cache')
	
	ssl_context = devtls.create_ssl_context()
	
	app = web.Application()
	app.on_startup.append(open_browser)
	app.add_routes([web.get('/', page)])
	web.run_app(app, port = 443, ssl_context = ssl_context)

async def open_browser(app: web.Application) -> None:
	import webbrowser
	webbrowser.open('https://localhost')

async def page(req: web.Request) -> web.Response:
	return web.Response(text = PAGE.format(url = req.url), content_type = 'text/html')

PAGE = """<!doctype html>
<meta charset="utf-8">
<title>DevTLS Test Server</title>
<body>
	<h1>DevTLS Test Server</h1>
	<p>If you're seeing this page, it means it works.</p>
	<p><small>Current URL: <a href="{url}">{url}</a></small></p>
</body>
"""

if __name__ == '__main__':
	main()
